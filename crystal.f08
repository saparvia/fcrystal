! Written by Stefan Parviainen 2012
! Public Domain where applicable. No rights reserved.
module crystal
real, parameter :: cubic_unit_cell(3,3) = reshape([ &
        1.0, 0.0, 0.0, &
        0.0, 1.0, 0.0, &
        0.0, 0.0, 1.0], [3,3])
real, parameter :: sc_basis(3,1) = reshape([0.0, 0.0, 0.0], [3,1])
real, parameter :: fcc_basis(3,4) = reshape([ &
        0.0,0.0,0.0, &
        0.0,0.5,0.5, &
        0.5,0.0,0.5, &
        0.5,0.5,0.0], [3,4])
real, parameter :: bcc_basis(3,2) = reshape([ &
        0.0, 0.0, 0.0, &
        0.5, 0.5, 0.5], [3,2])

contains
subroutine make_lattice(unit_vectors, lattice_size, lattice, basis)
implicit none
real, intent(in) :: unit_vectors(3,3)
integer, intent(in) :: lattice_size(3)
real, allocatable, intent(out) :: lattice(:,:)
real, optional, intent(in) :: basis(:,:)

integer :: atoms_per_unit_cell
integer :: i, j, k, natom

if(present(basis)) then
        atoms_per_unit_cell = size(basis,2)
else
        atoms_per_unit_cell = 1
end if

allocate(lattice(3, product(lattice_size)*atoms_per_unit_cell))
natom = 1
do k = 1, lattice_size(3); do j = 1, lattice_size(2); do i = 1, lattice_size(1)
        if(present(basis)) then
                lattice(:,natom:natom + atoms_per_unit_cell - 1) = ( spread(matmul(unit_vectors, &
                        [i,j,k]), 2, atoms_per_unit_cell) + basis )
        else
                lattice(:,natom) = (matmul(unit_vectors, [i,j,k]))
        end if
        natom = natom + atoms_per_unit_cell
end do; end do; end do
end subroutine

subroutine make_FCC(lattice_size, a, lattice)
implicit none
integer, intent(in) :: lattice_size(3)
real, intent(in) :: a
real, allocatable, intent(out) :: lattice(:,:)
call make_lattice(cubic_unit_cell*a, lattice_size, lattice, fcc_basis*a)
end subroutine

subroutine make_BCC(lattice_size, a, lattice)
implicit none
integer, intent(in) :: lattice_size(3)
real, intent(in) :: a
real, allocatable, intent(out) :: lattice(:,:)
call make_lattice(cubic_unit_cell*a, lattice_size, lattice, bcc_basis*a)
end subroutine

subroutine make_SC(lattice_size, a, lattice)
implicit none
integer, intent(in) :: lattice_size(3)
real, intent(in) :: a
real, allocatable, intent(out) :: lattice(:,:)
call make_lattice(cubic_unit_cell*a, lattice_size, lattice, sc_basis*a)
end subroutine

subroutine read_XYZ(filepath, lattice)
character(len=*), intent(in) :: filepath
real, allocatable, intent(out) :: lattice(:,:)

integer :: natoms
character(len=255) :: str

integer :: funit, i

open(newunit=funit,file=filepath, status='old')
read (funit, *) natoms, str

allocate(lattice(3,natoms))

do i = 1, natoms
        read (funit, *) str, lattice(:,i)
end do
close(funit)
end subroutine

subroutine write_XYZ(filepath, lattice)
character(len=*), intent(in) :: filepath
real, intent(in) :: lattice(:,:)

integer :: funit, i

open(newunit=funit,file=filepath)
write (funit, '(I0,/,A)') size(lattice, 2), 'Comment'
do i=1, size(lattice,2)
        write (funit, *) 'X', lattice(:,i)
end do
close(funit)

end subroutine
end module
