program crystal_example
use crystal
real, allocatable :: lattice(:,:)
call make_FCC([20,20,209], 3.61, lattice)
call write_XYZ('fcc.xyz', lattice)
end program
